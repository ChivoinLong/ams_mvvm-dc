//
//  ArticleService.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation
import Moya
import RxSwift

protocol ArticleServiceType: class {
    var provider: MoyaProvider<AMS> { get }
    
    func articles(page: Int, limit: Int, completion: @escaping ([Article]) -> Void)
}

class ArticleService: ArticleServiceType {
    var provider: MoyaProvider<AMS>
    
    init(provider: MoyaProvider<AMS>) {
        self.provider = provider
    }
    
    func articles(page: Int, limit: Int, completion: @escaping ([Article]) -> Void) {
        self.provider.request(AMS.articles(page, limit)) { (result) in
            switch result {
            case let .success(articles):
                completion(try! articles.map(Response<[Article]>.self).DATA!)
                
            case let .failure(error):
                print(error)
            }
        }
    }
}
