//
//  AppFlow.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/11/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation
import Swinject
import RxFlow

class AppFlow: Flow {
    
    var root: Presentable {
        return self.rootViewController
    }
    
    private let rootViewController = UINavigationController()
    private let container: Container
    
    init(container: Container) {
        self.container = container
    }
    
    func navigate(to step: Step) -> NextFlowItems {
        guard let step = step as? ArticleStep else { return NextFlowItems.none }
        
        switch step {
            
        case .articleList:
            return navigateToArticleListScreen()
            
        case .articleDetail(let article):
            return navigateToArticleDetailScreen(with: article)
        }
    }
    
    private func navigateToArticleListScreen() -> NextFlowItems {
        let viewModel = container.resolve(ArticleViewModelType.self)!
        let viewController = container.resolve(ArticleListVC.self)!
        
        viewController.viewModel = viewModel
        
        self.rootViewController.pushViewController(viewController, animated: true)
        
        return NextFlowItems.one(flowItem: NextFlowItem(nextPresentable: viewController, nextStepper: viewModel))
    }
    
    private func navigateToArticleDetailScreen(with article: Article) -> NextFlowItems {
        let viewController = container.resolve(ArticleDetailVC.self)!
        
        viewController.article = article
        
        self.rootViewController.pushViewController(viewController, animated: true)
        
        return NextFlowItems.one(flowItem: NextFlowItem(nextPresentable: viewController, nextStepper: viewController))
    }
}
