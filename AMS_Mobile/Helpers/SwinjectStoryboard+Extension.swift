//
//  SwinjectStoryboard+Extension.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Moya
import Swinject
import SwinjectStoryboard

let assembler = Assembler(container: SwinjectStoryboard.defaultContainer)

extension SwinjectStoryboard {
    
    @objc class func setup() {
        registryArticleListVC(container: defaultContainer)
        registryArticleDetailVC(container: defaultContainer)
    }
    
    class func registryArticleListVC(container: Container) {
        assembler.apply(assemblies: [ArticleListAssembly()])
    }
    
    class func registryArticleDetailVC(container: Container) {
        assembler.apply(assemblies: [ArticleDetailAssembly()])
    }
}
