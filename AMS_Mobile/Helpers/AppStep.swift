//
//  AppStep.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/11/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation
import RxFlow

enum ArticleStep: Step {
    
    case articleList
    case articleDetail(article: Article)
}
