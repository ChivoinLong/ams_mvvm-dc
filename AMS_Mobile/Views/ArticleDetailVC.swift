//
//  ArticleDetailVC.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import UIKit
import RxFlow

class ArticleDetailVC: UIViewController, Stepper {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var article: Article!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.titleLabel.text = article.TITLE
        self.descriptionLabel.text = article.DESCRIPTION
        self.dateLabel.text = article.CREATED_DATE
    }
}
