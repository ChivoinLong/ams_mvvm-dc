//
//  ArticleListVC.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ArticleListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    public var viewModel: ArticleViewModelType!
    private let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        
        self.viewModel.articleList
            .asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: UITableViewCell.self)) { (row, element, cell) in
                cell.textLabel?.text = element.TITLE
            }
            .disposed(by: disposeBag)
        
//        self.tableView.rx.willDisplayCell
//            .asObservable()
//            .subscribe(onNext: { [weak self] (cell, indexPath) in
//                guard let self = self else { return }
//                let lastIndex = self.viewModel.articleList.value.count - 1
//                
//                if indexPath.row == lastIndex {
//                    self.viewModel.fetchArticleList()
//                }
//            })
//            .disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected
            .bind(to: self.viewModel.detailAction.inputs)
            .disposed(by: self.disposeBag)
    }
}

