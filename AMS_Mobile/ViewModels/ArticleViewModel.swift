//
//  ArticleViewModel.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation
import RxFlow
import RxSwift
import Action

protocol ArticleViewModelType: Stepper {
    
    var articleService: ArticleServiceType { get set }
    var articleList: Variable<[Article]> { get set }
    var detailAction: Action<IndexPath, Swift.Never> { get set }
    var backAction: Action<Swift.Never, Swift.Never> { get set }
    
    func fetchArticleList() -> Void
}

class ArticleViewModel: ArticleViewModelType {
    
    var articleService: ArticleServiceType
    var articleList: Variable<[Article]> = Variable([])
    var page = 0
    
    lazy var detailAction: Action<IndexPath, Swift.Never> = {
        return Action { [weak self] indexPath in
            guard let self = self else { return .empty() }
            let article = self.articleList.value[indexPath.row]
            
            return self.detailViewController(article: article)
        }
    }()
    
    lazy var backAction: Action<Swift.Never, Swift.Never> = {
        return Action { [weak self] _ in
            guard let self = self else { return .empty() }
            
            return self.popViewController()
        }
    }()
    
    init(articleService: ArticleServiceType) {
        self.articleService = articleService
        fetchArticleList()
    }
    
    func fetchArticleList() {
        self.page += 1
        
        articleService.articles(page: self.page, limit: 10) { articles in
            self.articleList.value.append(contentsOf: articles)
        }
    }
    
    func detailViewController(article: Article) ->  Observable<Swift.Never> {
        self.step.accept(ArticleStep.articleDetail(article: article))
        
        return .empty()
    }
    
    func popViewController() ->  Observable<Swift.Never> {
        self.step.accept(ArticleStep.articleList)
        
        return .empty()
    }
}
