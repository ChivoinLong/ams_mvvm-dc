//
//  Article.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation

struct Article: Decodable {

    let ID: Int
    let TITLE: String
    let DESCRIPTION: String
    let CREATED_DATE: String
    let IMAGE: URL?
}
