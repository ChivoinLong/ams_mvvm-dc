//
//  Response.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation

public struct Response<T: Decodable>: Decodable {
    
    let DATA        : T?
    let CODE        : String
    let MESSAGE     : String
    let PAGINATION  : Pagination?
}


struct Pagination: Decodable {
    
    let PAGE    : Int
    let LIMIT   : Int
    let TOTAL_COUNT : Int
    let TOTAL_PAGES : Int
}
