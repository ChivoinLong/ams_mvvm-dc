//
//  AMS.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation
import Moya

enum AMS {
    
    static let Authorization: String = "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="
    
    case articles(_ page: Int, _ limit: Int)
}

extension AMS: TargetType {
    var baseURL: URL {
        return URL(string: "http://ams.chhaileng.com:8080/v1/api")!
    }
    
    var path: String {
        switch self {
        case .articles(_, _):
            return "/articles"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .articles(_, _):
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case let .articles(page, limit):
            return .requestParameters(parameters: [ "page": page, "limit": limit ], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .articles(_, _):
            return [ "Authorization" : AMS.Authorization ]
        }
    }
}
