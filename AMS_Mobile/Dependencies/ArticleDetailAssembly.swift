//
//  ArticleDetailAssembly.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/11/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation
import Swinject
import Moya

struct ArticleDetailAssembly: Assembly {
    
    func assemble(container: Container) {
        
//        container.register(ArticleServiceType.self) { (resolver) -> ArticleServiceType in
//            let provider = MoyaProvider<AMS>(plugins: [NetworkLoggerPlugin(verbose: true)])
//            return ArticleService(provider: provider)
//        }
//        
//        container.register(ArticleViewModelType.self) { (resolver) -> ArticleViewModelType in
//            return ArticleViewModel(articleService: resolver.resolve(ArticleServiceType.self)!)
//        }
        
        container.register(ArticleDetailVC.self) { (resolver) -> ArticleDetailVC in
            let vc =  UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
            return vc
        }
    }
}
