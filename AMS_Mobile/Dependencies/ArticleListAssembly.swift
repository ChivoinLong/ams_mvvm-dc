//
//  ArticleListAssembly.swift
//  AMS_Mobile
//
//  Created by LongChivoin on 1/11/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import Foundation
import Swinject
import Moya

struct ArticleListAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(ArticleServiceType.self) { (resolver) -> ArticleServiceType in
            let provider = MoyaProvider<AMS>(plugins: [NetworkLoggerPlugin(verbose: true)])
            return ArticleService(provider: provider)
        }
        
        container.register(ArticleViewModelType.self) { (resolver) -> ArticleViewModelType in
            return ArticleViewModel(articleService: resolver.resolve(ArticleServiceType.self)!)
        }
        
        container.register(ArticleListVC.self) { (resolver) -> ArticleListVC in
            let vc =  UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController() as! ArticleListVC
            return vc
        }
    }
}
