//
//  AMS_MobileTests.swift
//  AMS_MobileTests
//
//  Created by LongChivoin on 1/9/19.
//  Copyright © 2019 LongChivoin. All rights reserved.
//

import XCTest
import Swinject
@testable import AMS_Mobile

class AMS_MobileTests: XCTestCase {

    private let container = Container()
    
    override func setUp() {
        super.setUp()
        
        // register
    }

    override func tearDown() {
        super.tearDown()
        
        container.removeAll()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
